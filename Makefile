all: 2sf2wav

%.o: %.cpp $(wildcard desmume/*.h sseqplayer/*.h *.h spu/*.h) Makefile
	g++ -std=gnu++14 -O3 -c -I. -Idesmume/ -Isseqplayer/ -fpermissive -o $@ $<

2sf2wav: $(patsubst %.cpp, %.o, $(wildcard sseqplayer/*.cpp spu/*.cpp desmume/*.cpp desmume/*/*.cpp desmume/*/*/*.cpp desmume/utils/AsmJit/*/*.cpp *.cpp))
	g++ -o $@ $^ $(shell pkg-config --libs zlib)

clean:
	rm -f 2sf2wav *.o sseqplayer/*.o spu/*.o desmume/*.o desmume/metaspu/*.o desmume/addons/*.o desmume/utils/*.o desmume/utils/AsmJit/base/*.o desmume/utils/AsmJit/x86/*.o
